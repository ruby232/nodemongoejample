import express from 'express';
import cors from 'cors';
import * as bodyParser from 'body-parser';
import mongoConnect from './mongo-connect';
import { routes } from './routes';
import {Controller} from "./controller";

// Initial mongo
mongoConnect();

// Initial express
const app = express();
app.use(bodyParser.json());
app.use(cors({ origin: true }));

// Configure routes of express
routes(app, new Controller(), 'projects')

// set port, listen for requests
const PORT = process.env.PORT || 3030;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});

