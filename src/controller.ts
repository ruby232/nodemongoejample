import {Request, Response} from "express";

export class Controller {
    index(req: Request, res: Response) {
        try {
            res.status(200).send({data: 'Hola'})
        } catch (err) {
            res.status(500).send({message: `${err.code} - ${err.message}`});
        }
    }
}
