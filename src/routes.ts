import {Application, Request, Response} from "express";
import Router from "express-promise-router";
import {Controller} from "./controller";


export function routes(app: Application, controller: Controller, path: string) {
    const router = Router();
    app.use("/" + path, router);

    router.get("/",
        (req: Request, res: Response) =>{  controller.index(req,res )}
    );
}
