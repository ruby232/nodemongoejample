import mongoose from 'mongoose';

export default () => {

    const MONGO_USER = process.env.MONGO_USERNAME || 'root';
    const MONGO_PASSWORD = process.env.MONGO_PASSWORD || 'PassRootMongo';
    const MONGO_HOST = process.env.MONGO_HOST || 'mongo';
    const MONGO_DB = process.env.MONGO_DB || 'mongo_db';

    const url = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:27017/${MONGO_DB}?authSource=admin&w=1`;

    const mongoConnect = () => {
        mongoose.connect(
            url,
            {
                useUnifiedTopology: true,
                useNewUrlParser: true,
                useCreateIndex: true
            }
        )
            .then(() => {
                return console.info(`Successfully connected to ${url}`);
            })
            .catch(error => {
                console.error('Error connecting to database: ', error);
                return process.exit(1);
            });
    };
    mongoConnect();
    mongoose.connection.on('disconnected', mongoConnect);
};
