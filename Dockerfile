FROM node:15.2.1-alpine3.12

ENV APP_DIR=/var/app
WORKDIR $APP_DIR

COPY . .

RUN npm install && \
    chmod +x entrypoint.sh && \
    npm run build

EXPOSE 3030
ENTRYPOINT ["/var/app/entrypoint.sh"]
CMD ["npm", "run", "start"]

